/***************************************************************************
 select_function.c
 -----------------
 This is select_function: function pointers are assigned here
 -------------------
 begin                : Thu Apr 25 2002
 copyright            : (C) 2002 by Mario Ricchiuto
 email                : ricchiut@vki.ac.be
 ***************************************************************************/
#include "common.h"

extern int problem ;
extern int scheme, iteration_max, out_format, in_format, mesh_format, exact, exact_format ;

extern void void_function() ;

extern void ( *supersonic_residual )( int ) ; /* node global index  */

extern void supersonic_residual_std( int ) ;
extern void supersonic_residual_2DRP_Euler( int ) ;
extern void supersonic_residual_2DRPiso_Euler( int ) ;
extern void supersonic_residual_dMach_Euler( int ) ;
extern void supersonic_residual_q1d( int ) ;


/**************************************************************************/
/**                   SCHEME dependent functions                         **/
/**************************************************************************/

extern void ( *distribute_fluctuation )( int ) ;

extern void LDA_unsteady( int ) ;
extern void LLF_unsteady( int ) ;
extern void LDA_N_unsteady( int ) ;
extern void LW_LF_unsteady( int ) ;
extern void LW_unsteady( int ) ;
extern void N_unsteady( int ) ;


extern void ( *fluctuation2 )( int ) ;

extern void fluctuationRK1( int ) ;
extern void fluctuationRK2( int ) ;
extern void fluctuationRK3( int ) ;

extern void ( *move_the_mesh )( int ) ;

extern void sinusoidal( int ) ;
extern void flap_deflection( int ) ;
extern void centered_compression( int) ;


/**************************************************************************/
/**         INPUT/OUTPUT  dependent functions                 **/
/**************************************************************************/

extern void ( *write_solutions )() ;

extern void write_solution_ascii_tec() ;
extern void write_solution_meshb() ;
extern void write_solution_ascii_paraview() ;

extern void ( *read_solutions )() ;

extern void read_solution_ascii_tec() ;
extern void read_solution_meshb() ;
extern void read_solution_ascii_paraview() ;

extern void ( *read_exact_solutions )() ;

extern void read_exact_solution_meshb() ;

extern void ( *compute_exact_solution )() ;

extern void compute_shockednozzle_exact_solution() ;
extern void compute_rectangularreflexion_exact_solution() ;

extern void ( *compute_initial_solution )() ;

extern void initial_reference_state() ;

extern void ( *mesh_reader )() ;

extern void read_spatial_grid_grd() ;
extern void read_spatial_grid_meshb() ;

extern void ( *mesh_header_read )() ;

extern void read_spatial_grid_header_grd() ;
extern void read_spatial_grid_header_meshb() ;



/**************************************************************************/
/**************************************************************************/
/**        here begins SELECT_FUNCTION                         **/
/**************************************************************************/
/**************************************************************************/

void select_function()
{
    /**************************************************************************/
    /**     PROBLEM DEPENDENT SETTINGS !!!                      **/
    /**************************************************************************/
    
    if ( exact ) {
        switch ( exact_format ) {
            case -1 :
                break;
            case 1 :
                read_exact_solutions = read_exact_solution_meshb;
                break ;
            default :
                printf("ERROR  can only read exact solution in .mesh(b) format.\n");
                exit(1);
        }
        
        if (exact_format == -1) {
            switch ( problem ) {
                case 9:
                    compute_exact_solution = compute_rectangularreflexion_exact_solution;
                    break;
                case 6:
                case 7:
                case 8:
                    compute_exact_solution = compute_shockednozzle_exact_solution;
                    break;
                default:
                    printf("ERROR  the analytic solution is only known for the rectangular reflexion and Q1D cases.\n");
                    exit(1);
            }
        }
    }
    
    supersonic_residual = supersonic_residual_std;
    switch ( problem )
    {
        case 1: /*Supersonic cyrcular Cylinder*/
            printf( "Supersonic cyrcular Cylinder: CASE NOT READY\n" ) ;
            move_the_mesh =  void_function ;
            printf( "  ALE NOT READY\n" ) ;
            break ;
        case 2: /*Coalescing Shocks*/
            printf( "Coalescing Shocks: CASE NOT READY\n" ) ;
            move_the_mesh =  void_function ;
            printf( "  ALE NOT READY\n" ) ;
            break ;
        case 3: /*Mach Reflaction*/
            printf( "Mach Reflaction: CASE NOT READY\n" ) ;
            move_the_mesh =  void_function ;
            printf( "  ALE NOT READY\n" ) ;
            break ;
        case 4: /*NACA0012_M080_A0*/
            printf( "NACA0012_M080_A0: CASE NOT READY\n" ) ;
            move_the_mesh =  void_function ;
            printf( "  ALE NOT READY\n" ) ;
            break ;
        case 5: /*NACA0012_M095_A0_fishtail-1*/
            printf( "NACA0012_M095_A0_fishtail-1: CASE NOT READY\n" ) ;
            move_the_mesh =  void_function ;
            printf( "  ALE NOT READY\n" ) ;
            break ;
        case 6: /*Quasi 1D nozzle flow*/
        case 7: /*Quasi 1D nozzle flow*/
        case 8: /*Quasi 1D nozzle flow*/
            printf( "Quasi 1D nozzle flow: CASE NOT READY\n" ) ;
            move_the_mesh =  void_function ;
            supersonic_residual = supersonic_residual_q1d;
            printf( "  ALE NOT READY\n" ) ;
            break ;
        case 9: /*Regular Reflection*/
            printf( "Regular Reflection: CASE NOT READY\n" ) ;
            move_the_mesh =  void_function ;
            printf( "  ALE NOT READY\n" ) ;
            break ;
        case 10: /*SSInteractions1*/
            printf( "SSInteractions1: CASE NOT READY\n" ) ;
            move_the_mesh =  void_function ;
            printf( "  ALE NOT READY\n" ) ;
            break ;
        case 11: /*SSInteractions2*/
            printf( "SSInteractions2: CASE NOT READY\n" ) ;
            move_the_mesh =  void_function ;
            printf( "  ALE NOT READY\n" ) ;
            break ;
        case 12: /*Vortex_isoRho_steady*/
            printf( "Vortex_isoRho_steady: CASE NOT READY\n" ) ;
            move_the_mesh =  void_function ;
            printf( "  ALE NOT READY\n" ) ;
            break ;
        case 13: /*Vortex_isoRho_moving*/
            printf( "Vortex_isoRho_moving: CASE NOT READY\n" ) ;
            move_the_mesh =  void_function ;
            printf( "  ALE NOT READY\n" ) ;
            break ;
        case 14: /*DoubleNACA_transonic*/
            printf( "DoubleNACA_transonic: CASE NOT READY\n" ) ;
            move_the_mesh =  void_function ;
            printf( "  ALE NOT READY\n" ) ;
            compute_initial_solution = initial_reference_state;
            break ;
    }
    
    /**************************************************************************/
    /**           SCHEME  dependent settings                         **/
    /**************************************************************************/
    
    switch ( scheme ) {
    case 0:
        distribute_fluctuation = LDA_unsteady ;
        break ;
    case 1:
        distribute_fluctuation = LLF_unsteady ;
        break ;
    case 2:
        distribute_fluctuation = LDA_N_unsteady ;
        break ;
    case 3:
        distribute_fluctuation = LW_LF_unsteady ;
        break ;
    case 4:
        distribute_fluctuation = LW_unsteady ;
        break ;
    case 5:
        distribute_fluctuation = N_unsteady ;
        break ;
    }
    
    switch ( iteration_max ) {
    case 1 :
        fluctuation2 = fluctuationRK1 ;
        break ;
    case 2 :
        fluctuation2 = fluctuationRK2 ;
        break ;
    case 3 :
        fluctuation2 = fluctuationRK3 ;
        break ;
    }
    
    
    switch ( out_format ) {
    case 0 :
        write_solutions = write_solution_ascii_tec ;
        break ;
    case 1 :
        write_solutions = write_solution_meshb ;
        break ;
    case 2 :
        write_solutions = write_solution_ascii_paraview ;
        break ;
    default :
        printf("ERROR  can only write solution files in .dat (ascii tec), .vtk or .sol(b) format.\n");
        exit(1);
    }
    
    switch ( in_format ) {
    case 0 :
        read_solutions  = read_solution_ascii_tec ;
        break ;
    case 1 :
        read_solutions = read_solution_meshb ;
        break ;
    case 2 :
        read_solutions  = read_solution_ascii_paraview ;
        break ;
    default :
        printf("ERROR  can only read solution files in .dat (ascii tec), .vtk or .sol(b) format.\n");
        exit(1);
    }
    
    switch ( mesh_format ) {
    case 0 :
        mesh_reader      = read_spatial_grid_grd ;
        mesh_header_read = read_spatial_grid_header_grd ;
        break ;
    case 1 :
        mesh_reader      = read_spatial_grid_meshb ;
        mesh_header_read = read_spatial_grid_header_meshb ;
        break ;
    default :
        printf("ERROR  can only read mesh files in .grd or .mesh(b) format.\n");
        exit(1);
    }

}
/**************************************************************************/
/**************************************************************************/
/**         here ends SELECT_FUNCTION                         **/
/**************************************************************************/
/**************************************************************************/

/***************************************************************************
 *   This program has been developed at the von Karman Institute for Fluid *
 *   Dynamics by Mario Ricchiuto. Any change or update must be done in a   *
 *   clean way and it must be clearly commented.                           *
 *   Mario Ricchiuto                                                       *
 *                                              23/04/2002                 *
 ***************************************************************************/

