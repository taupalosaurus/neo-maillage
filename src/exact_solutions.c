#include "common.h"

extern int NN;
extern struct node_struct *node ;
extern void (*write_solutions)() ;

double gam = 1.4;
double gm1 = 0.4;
double gp1 = 2.4;
double rgas = 287.185;
//double pi = 3.14159265358979323846;
double deg2grd = pi/180.;


/*************************************************/
/*     Rectangular Reflexion (problem 0)         */
/*************************************************/

void rectangular_reflexion(double x, double y, double *rho, double *vel, double *p)
{
    
    double uref, rref, pref;
    double mincn1, mincn2, mrefn1, mrefn2;
    double pratio, tratio, rratio;
    double amach1, p1, t1, r1, u1, v1, k1, h1;
    double amach2, p2, t2, r2, u2, v2, k2, h2;
    double amach3, p3, t3, r3, u3, v3, k3, h3;
    double xin, yin;
 
    // State 1
    amach1 = 2.0;
    p1 = 1.e5;
    t1 = 300.;
    r1 = p1 / (rgas*t1);
    u1 = amach1 * sqrt(gam*rgas*t1);
    v1 = 0.;

    uref = abs(u1);
    rref = r1;
    pref = rref*uref*uref;

    p1 = p1 / pref;
    u1 = u1 / uref;
    v1 = v1 / uref;
    k1 = 0.5*( u1*u1 + v1*v1 );
    r1 = r1 / rref;
    h1 = gam/(gm1)* p1/r1 + k1;

    // incident shock
    pratio = 1.70657860;
    tratio = 1.17015128;
    rratio = 1.45842561;
    mincn1 = 1.26713803;
    mincn2 = 0.80319063;

    // State 2
    amach2 = 1.64052221;
    p2 = p1 * pratio;
    t2 = t1 * tratio;
    r2 = r1 * rratio;
    u2 = amach2 * sqrt(gam*p2/r2) * cos(-10. * deg2grd);
    v2 = amach2 * sqrt(gam*p2/r2) * sin(-10. * deg2grd);
    k2 = 0.5*( u2*u2 + v2*v2 );
    h2 = gam/(gm1)* p2/r2 + k2;

    // reflected shock
    pratio = 1.64257921;
    tratio = 1.15642488;
    rratio = 1.42039421;
    mrefn1 = 1.24530405;
    mrefn2 = 0.81528188;

    // State 3
    amach3 = 1.28488929;
    p3 = p2 * pratio;
    t3 = t2 * tratio;
    r3 = r2 * rratio;
    u3 = amach3 * sqrt(gam*p3/r3);
    v3 = 0.;
    k3 = 0.5*( u3*u3 + v3*v3 );
    h3 = gam/(gm1)* p3/r3 + k3;

    if ( (y <= (-1./1.2*x + 1 )) && (x<=1.2)) {
        // state 1
        *rho = r1;
        vel[0] = u1;
        vel[1] = v1;
        *p = p1;
    }
    else if ( (y>=(-1./1.2*x + 1.  )) && (y>=(1./1.2*x - 1.))) {
        // state 2
        *rho = r2;
        vel[0] = u2;
        vel[1] = v2;
        *p = p2;
    }
    else {
        //state 3
        *rho = r3;
        vel[0] = u3;
        vel[1] = v3;
        *p = p3;
    }
}


void compute_rectangularreflexion_exact_solution()
{
    double rho, p, u[2], x, y;

    for (int n=0; n<NN; ++n) {
        x = node[n].coordinate[0];
        y = node[n].coordinate[1];
        rectangular_reflexion(x, y, &rho, u, &p);
        node[n].exact_sol[0] = rho;
        node[n].exact_sol[1] = u[0];
        node[n].exact_sol[2] = u[1];
        node[n].exact_sol[3] = p;
    }
}

void write_rectangularreflexion()
{
    double rho, p, u[2], x, y;

    for (int n=0; n<NN; ++n) {
        x = node[n].coordinate[0];
        y = node[n].coordinate[1];
        rectangular_reflexion(x, y, &rho, u, &p);
        node[n].Z[2][0] = rho;
        node[n].Z[2][1] = u[0];
        node[n].Z[2][2] = u[1];
        node[n].Z[2][3] = p;
    }
    write_solutions();
    exit(1);
}


/*************************************************/
/*        Shocked nozzle (problem 8)             */
/*************************************************/

double aratio(double mach) 
{
	double espn, temp, aratio;

    espn = 0.5*gp1/gm1;
    temp = 1. + 0.5 * gm1 * (mach*mach);
    aratio = pow((2./gp1*temp),espn);
    aratio = aratio/mach;
    
    return aratio;
}

double pstat(double mach) 
{
	double espn, temp, pstat;

    espn = -gam/gm1;
    temp = 1. + 0.5 * gm1 * (mach*mach);
    pstat = pow(temp, espn);

    return pstat;
}

double rstat(double mach)
{
	double espn, temp, rstat;

    espn = -1./gm1;
    temp = 1. + 0.5 * gm1 * (mach*mach);
    rstat = pow(temp,espn);

    return rstat;
}

double tstat(double mach)
{
	double temp, tstat;

    temp = 1. + 0.5 * gm1 * (mach*mach);
    tstat = 1./temp;

    return tstat;
}

static inline double dg(double x, double ratio)
{
    double dg;

    dg = ratio - aratio(x);

    return dg;
}

double secanti(double a0, double b0, double ratio)
{
	int i, maxits;
    double TOLER, x, a, b;

    maxits = 390;
    TOLER = 1.e-10;
    a = a0; b = b0;

    if (dg(a, ratio)*dg(b, ratio) >= 0) {
        exit(2);
    }

    for (int i=0; i<maxits; ++i) {
        x = a - (b-a)*dg(a, ratio)/(dg(b, ratio)-dg(a, ratio));
        if (abs(dg(x, ratio)) <= TOLER){
            return x;
        }
        if(dg(x, ratio)*dg(a, ratio) <= 0.0) {
            b = x;
        }
        else {
            a = x;
        }
    }

    exit(4);
}

// mach number behind the NORMAL shock
double am2(double mach) 
{
    double tempa, tempb, am2;

    tempa = 1. + 0.5*gm1*(mach*mach);
    tempb = gam*mach*mach - 0.5*gm1;
    am2 = sqrt(tempa/tempb);

    return am2;
}

// static pressure ratio across the NORMAL shock
double pjump(double mach)
{
  	double pjump;

    pjump = 1. + 2.*gam/gp1 * ( mach*mach - 1.);

    return pjump;
}

// total pressure ratio across the NORMAL shock
double p0jump(double mach1)
{
    double mach2, temp2, temp1, p0jump;

    mach2 = am2(mach1);
    temp2 = 1. + 0.5*gm1*(mach2*mach2);
    temp1 = 1. + 0.5*gm1*(mach1*mach1);
    p0jump = pjump(mach1) * pow((temp2/temp1),(gam/gm1));

    return p0jump;
}

static inline double F(double X, double ratio)
{
	double F;

    F=(((1.+0.2*X*X)/1.2)*((1.+0.2*X*X)/1.2)*((1.+0.2*X*X)/1.2))/X-ratio;
    
    return F;
}

static inline double F1(double X)
{
	double HELP, F1;

    HELP = (1.+0.2*X*X)/1.2;
    F1 = HELP*HELP*(1.-HELP/(X*X));

    return F1;
}

double XEWRAP(double P0, double Delta, double Epsilon, int Max, double ratio)
{    
	double Small, Y0, P1, Df, Dp, Y1, re;

    Small = 1e-20;

    Y0 = F(P0,ratio);
    P1 = P0+1;
    for (int K=0; K<Max; ++K) {
        Df = F1(P0);
        if (fabs(Df) <= Small) {
            Dp = P1-P0;
            P1 = P0;
            break;
        }
        else {
            Dp = Y0/Df;
            P1 = P0-Dp;
        }
        Y1 = F(P1, ratio);
        re = fabs(Dp)/(fabs(P1)+Small);
        P0 = P1;
        Y0 = Y1;
    }

    return P1;
}


void shockednozzle(double x, double y, double *rho, double *vel, double *p)
{
	double PETSC_PI, EPS, x0, y0, th0, Delta, Epsilon, GA, GM1;
	double amachi, help, rref, tref, pref, rsh, ratio, a, b, am1sh, am2sh;
	double astaratio, amach, pres, h, P, p0r, P1, dens, z1, temp, uth, u, v;
	int Max;

    PETSC_PI = 3.14159265358979323846264;
    EPS = 0.001;
    x0 = 1.4;
    y0 = 0.6;
    th0 = 15.*PETSC_PI/180.;
    Delta = 1E-13;
    Epsilon = 1E-13;
    Max = 500;
    GA = 1.4;
    GM1 = GA-1.;

    amachi = 2.0;
    help = aratio(amachi); 
    rref = rstat(amachi);
    tref = tstat(amachi);
    pref = GA*amachi*amachi*pstat(amachi);

    rsh = 1.5;
    ratio = rsh * aratio(amachi);

    // find the supersonic Mach number just ahead of the shock
    a = 1.05;
    b = 3.;
    am1sh = secanti(a, b, ratio);

    // changes through the shock
    am2sh = am2(am1sh);
    astaratio = 1./p0jump(am1sh); 
    ratio = 2. / astaratio * aratio(amachi);
    a = 0.05;
    b = 0.99;
    amach = secanti(a, b, ratio);
    pres = pstat(amach) / astaratio / pref;
      
    h = sqrt(x*x+y*y);
    if (h <= rsh) {
        ratio = h * aratio(amachi);
        P = 1.5;
        p0r = 1.;
    }
    else {
        p0r = 1./astaratio;
        ratio = h * aratio(amachi) * p0r;
        P = 0.5;
    }
    P1 = XEWRAP(P, Delta, Epsilon, Max, ratio);
    amach = P1;
    P = P1;

    dens = rstat(amach) / rref * p0r;
    pres = pstat(amach) / pref * p0r;
    
    z1 = sqrt(dens);
    temp = tstat(amach) / tref;
    uth = (amach/amachi) * sqrt(temp);
    u = z1 * uth * x / (h*z1);
    v = z1 * uth * y / (h*z1);

    *rho = dens;
    vel[0] = u;
    vel[1] = v;
    *p = pres;
}


void compute_shockednozzle_exact_solution()
{
    double rho, p, u[2], x, y;

    for (int n=0; n<NN; ++n) {
        x = node[n].coordinate[0];
        y = node[n].coordinate[1];
        shockednozzle(x, y, &rho, u, &p);
        node[n].exact_sol[0] = rho;
        node[n].exact_sol[1] = u[0];
        node[n].exact_sol[2] = u[1];
        node[n].exact_sol[3] = p;
    }
}

void write_shockednozzle()
{
    double rho, p, u[2], x, y;

    for (int n=0; n<NN; ++n) {
        x = node[n].coordinate[0];
        y = node[n].coordinate[1];
        shockednozzle(x, y, &rho, u, &p);
        node[n].Z[2][0] = rho;
        node[n].Z[2][1] = u[0];
        node[n].Z[2][2] = u[1];
        node[n].Z[2][3] = p;
    }
    write_solutions();
    exit(1);
}