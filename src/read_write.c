/***************************************************************************
                                 read_write.c
                                 ------------
            This is read_write: it contains every function reading
                           or writing the solution
                             -------------------
    begin                : Mon May 6 2002
    copyright            : (C) 2002 by Mario Ricchiuto
    email                : ricchiut@vki.ac.be
***************************************************************************/
#include "common.h"
#include "libmeshb7.h"

extern int NN, NE, movie, time_step, save_solution ;
extern int NBF, initial_state ;

extern double gm, time ;

extern struct element_struct   *element ;
extern struct node_struct      *node ;
extern struct boundary_struct *boundary ;

extern char out_file[MAX_CHAR] ;
extern char initial_solution_file[MAX_CHAR] ;

extern void Z_2_P( double *, double * ) ;

FILE *out, *checkout ;
FILE *initial ;

/*********************************************************/
/*********************************************************/
/**         TECPLOT reader/writer                **/
/*********************************************************/
/*********************************************************/

void write_solution_ascii_tec()
{
    char tecplot_file_name[MAX_CHAR] ;
    char check_tecplot_file_name[MAX_CHAR] ;

    int e, n, TOT_N, TOT_E, lev, NNN ;
  
    double rho, u, v, p, Ma, s, T, en, h1, h2, xc, yc, omega, mm, r, pm, dp, h0 , H;
    
    lev   = 2 ;
    TOT_N = NN ;
    TOT_E = NE ;
  
    printf( "          *************************************\n" ) ;
    printf( "          **     Writing tecplot file.....   **\n" ) ;
    printf( "          *************************************\n" ) ;
    printf( "\n"                                                ) ;


    if ( movie ) {
        if (time_step < 100000) {
            sprintf( tecplot_file_name, "%s0%d.dat", out_file, time_step ) ;
        }
        if (time_step < 10000) {
            sprintf( tecplot_file_name, "%s00%d.dat", out_file, time_step ) ;
        }
        if (time_step < 1000) {
            sprintf( tecplot_file_name, "%s000%d.dat", out_file, time_step ) ;
        }
        if (time_step < 100) {
            sprintf( tecplot_file_name, "%s0000%d.dat", out_file, time_step ) ;
        }
        if (time_step < 10){
            sprintf( tecplot_file_name, "%s00000%d.dat", out_file, time_step ) ;    
        }
    }
    else { 
        sprintf( tecplot_file_name, "%s.dat", out_file ) ;
    }

    FILE *fp;
    fp = fopen("check.dat", "w");
    fprintf( fp, "TITLE      =  Unstructured grid data \n"                                 ) ;
    fprintf( fp, "VARIABLES  =  x  y  a b c d \n"                              ) ;
    fprintf( fp, "ZONE    N  =  %d    E  =  %d    F = FEPOINT    ET = TRIANGLE \n", TOT_N, TOT_E ) ;
    fprintf( fp, "\n"            ) ;
    for ( n = 0 ; n < TOT_N ; n++ ) {
     fprintf( fp, "%.20f %.20f %.20f %.20f %.20f %.20f\n",node[n].coordinate[0], node[n].coordinate[1], node[n].P[2][0],node[n].P[2][1],node[n].P[2][2],node[n].P[2][3] ) ;
    }
    for ( e = 0 ; e < NE ; e++ ) {
        fprintf( fp, "%d %d %d \n", element[e].node[0] + 1,
                                    element[e].node[1] + 1,
                                    element[e].node[2] + 1 ) ; 
    }
    fclose(fp) ;

    out = fopen( tecplot_file_name, "w" ) ;

    fprintf( out, "TITLE      =  Unstructured grid data \n"                                 ) ;
    fprintf( out, "VARIABLES  =  x  y  rho  u  v p H  Ma  s en time  sensor\n"                              ) ;
    fprintf( out, "ZONE    N  =  %d    E  =  %d    F = FEPOINT    ET = TRIANGLE \n", TOT_N, TOT_E ) ;
    fprintf( out, "\n"                                                                      ) ;
  
    if  ( initial_state == 9 ) {
        h1 = h2 = 0. ;
    }

    for ( n = 0 ; n < TOT_N ; n++ ) {
        rho = node[n].P[lev][0] ; rho = MAX( rho, 0.00000000000000000001 ) ;
        u   = node[n].P[lev][1]/rho ;
        v   = node[n].P[lev][2]/rho ;
        en  = node[n].P[lev][3]/rho - 0.50000000000000000000*( u*u + v*v ) ;
      
        p = ( gm - 1.0000000000000000000 )*rho*en ; p = MAX( p, 0.00000000000000000001 ) ;
        T = gm*en ;
      
        T *= gm/( gm - 1.00000000000000000000 ) ;
        s   = p/pow( rho, gm ) ;
        Ma  = sqrt( u*u + v*v )/sqrt( gm*p/rho ) ;
        H = (gm*p)/((gm-1.00000000000000000000)*rho) + (u*u + v*v)*0.50000000000000000000  ;
      
        // fprintf( out, "%le %le %le %le %le %le %le %le %le %le %le %le\n",node[n].coordinate[0], node[n].coordinate[1], rho, u, v, p, H, Ma, s, T, time, node[n].tt ) ;
        // fprintf( out, "%Lf %Lf %Lf %Lf %Lf %Lf %Lf %Lf %Lf %Lf %Lf %Lf\n",node[n].coordinate[0], node[n].coordinate[1], rho, u, v, p, H, Ma, s, T, time, node[n].tt ) ;
        // fprintf( out, "%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n",fprintf( out, "%Lf %Lf %Lf %Lf %Lf %Lf %Lf %Lf %Lf %Lf %Lf %Lf\n",node[n].coordinate[0], node[n].coordinate[1], rho, u, v, p, H, Ma, s, en, time, node[n].tt ) ;
        fprintf( out, "%.20f %.20f %.20f %.20f %.20f %.20f %.20f %.20f %.20f %.20f %.20f %.20f\n",node[n].coordinate[0], node[n].coordinate[1], rho, u, v, p, H, Ma, s, en, time, node[n].tt ) ;

        // printf( "%.20f %.20f %.20f %.20f %.20f %.20f\n",  node[n].P[2][0],node[n].P[2][1],node[n].P[2][2],node[n].P[2][3] ) ;
        // printf(checkout,"check me out\n") ;

        if ( initial_state == 9 ) {
            xc = node[n].coordinate[0] - 0.5 ;  
            yc = node[n].coordinate[1] - 0.5 ;  
            r  = sqrt( xc*xc + yc*yc ) ;
            pm = 100. ; 
            mm = p - pm ;
            dp = 0. ;
      
            if ( r <= 0.25 ) {
                omega = 4.*pi*r ;
                omega = 15.*( 1. + cos( omega ) ) ;              
                r     = 4.*pi*r ;
                dp    = ( 225.*1.4/( 16.*pi*pi ) )*( 2.*cos( r ) + 2.*r*sin( r ) + cos( 2.*r )/8. + r*sin( 2.*r )/4. + r*r*12./16. ) ;
                dp   -= ( 225.*1.4/( 16.*pi*pi ) )*( 2.*cos( pi ) + 2.*pi*sin( pi ) + cos( 2.*pi )/8. + pi*sin( 2.*pi )/4. + pi*pi*12./16. ) ;
            }
      
            mm -= dp ;                       
      
            mm = fabs( mm )/pm ;
      
            if ( mm > h0 ) {
                h0 = mm  ;
            }
            h1 += mm/(1.*NN) ;
            h2 += mm*mm/(1.*NN) ;
        }
    }   
    
    if (initial_state == 9 ) {
        h2 = sqrt( h2 ) ;
    
        printf("Errors\n") ;
        printf("Infty: %le\n",log10(h0)) ;
        printf("one: %le\n",log10(h1)) ;
        printf("two: %le\n",log10(h2)) ;
        printf("1/40: %le\n",log(1./40.)) ;
        printf("1/80: %le\n",log(1./80.)) ;
        printf("1/160: %le\n",log(1./160.)) ;
        printf("1/200: %le\n",log(1./200.)) ;
        printf("Ntot:%d\n",TOT_N) ;
    }

  
    for ( e = 0 ; e < NE ; e++ ) {
        fprintf( out, "%d %d %d \n", element[e].node[0] + 1,
                                     element[e].node[1] + 1,
                                     element[e].node[2] + 1 ) ;
    }
  
    fclose( out ) ;
}

//+++++++++++++++++++++++++++++++++++++++++++++++


void read_solution_ascii_tec()
{
    char init_file_name[MAX_CHAR] ;
    int ret, n, dummy1, dummy2, NTOT ;
    double rho, u, v, p, Ma, s, T, dummy3, dummy4, dummy5, dummy6, H;
    // 11111111111111111111111111111111111111111111111111111111111
    int e, TOT_N, TOT_E, NNN ;
    double en, h1, h2, xc, yc, omega, mm, r, pm, dp, h0;
    TOT_N = NN;
    // 1111111111111111111111111111111111111111111111111111111111
  
  
    printf( "          *************************************\n" ) ;
    printf( "          **     Reading tecplot file.....   **\n" ) ;
    printf( "          *************************************\n" ) ;
    printf( "\n"                                                ) ;
  
    sprintf( init_file_name, "%s.dat", initial_solution_file ) ;

    initial = fopen( init_file_name, "r" ) ;
  
    if (initial == NULL) {
        printf("Pb ouverture fichier" );
    }
  
    NTOT = NN ;
  
    ret = fscanf(initial,"TITLE      =  Unstructured grid data \n");
    ret = fscanf(initial,"VARIABLES  =  x  y  rho  u  v p H  Ma  s T time  sensor\n");
    ret = fscanf(initial,"ZONE    N  =  %d    E  =  %d     F = FEPOINT    ET = TRIANGLE \n",&dummy1, &dummy2);
    ret = fscanf(initial, "\n") ;
  
    for ( n = 0 ; n < NTOT ; n++ ) {

        // fscanf(initial, "%le %le %le %le %le %le %le %le %le %le %le %le\n", &dummy3, &dummy4, &rho, &u, &v, &p, &H, &Ma, &s, &T, &dummy5, &dummy6);
        // fscanf(initial, "%Lf %Lf %Lf %Lf %Lf %Lf %Lf %Lf %Lf %Lf %Lf %Lf\n", &dummy3, &dummy4, &rho, &u, &v, &p, &H, &Ma, &s, &T, &dummy5, &dummy6);
        ret = fscanf(initial, "%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n", &dummy3, &dummy4, &rho, &u, &v, &p, &H, &Ma, &s, &T, &dummy5, &dummy6);
    
        node[n].P[2][0] = rho ;
        node[n].P[2][1] = u*rho ;
        node[n].P[2][2] = v*rho ;
        node[n].P[2][3] = p/( gm - 1.0000000000000000000) + 0.50000000000000000000*rho*( u*u + v*v ) ;
    }   
    fclose(initial); 
}


/*
void write_initial_solution()
{
    char tecplot_file_name[MAX_CHAR] ;
    int e, n, TOT_N, TOT_E ;
    double rho, u, v, p, Ma, s, T, en ;
    TOT_N = NN ;
    TOT_E = NE ;
  
   printf( "          *************************************\n" ) ;
    p rintf( "          **     Writing tecplot file.....   **\n" ) ;
    printf( "          *************************************\n" ) ;
    printf( "\n"                                                ) ;
  
    sprintf( tecplot_file_name, "initial_solution.dat" ) ;
    out = fopen( tecplot_file_name, "w" ) ;

    fprintf( out, "TITLE      =  Unstructured grid data \n"                                 ) ;
    fprintf( out, "VARIABLES  =  x  y  rho  u  v  p  Ma  s  T \n"                              ) ;
    fprintf( out, "ZONE    N  =  %d    E  =  %d    F = FEPOINT    ET = TRIANGLE \n", TOT_N, TOT_E ) ;
    fprintf( out, "\n"                                                                      ) ;
  
    for ( n = 0 ; n < TOT_N ; n++ ) {
        rho = node[n].P[2][0] ; rho = MAX( rho, 0.00000000000000000001 ) ;
        u   = node[n].P[2][1]/rho ;
        v   = node[n].P[2][2]/rho ;
        en  = node[n].P[2][3]/rho - 0.5000000000000000000*( u*u + v*v ) ;
      
        p = ( gm - 1.00000000000000000000 )*rho*en ; p= MAX( p , 0.00000000000000000001 ) ;
        T = gm*en ;
      
        T *= gm/( gm - 1.00000000000000000000 ) ;
        s   = p/pow( rho, gm ) ;
        Ma  = sqrt( u*u + v*v )/sqrt( gm*p/rho ) ;
      
        // fprintf( out, "%le %le %le %le %le %le %le %le %le\n", node[n].coordinate[0], node[n].coordinate[1], rho, u, v, p, Ma, s, T ) ;
        // fprintf( out, "%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n", node[n].coordinate[0], node[n].coordinate[1], rho, u, v, p, Ma, s, T ) ;
        // fprintf( out, "%Lf %Lf %Lf %Lf %Lf %Lf %Lf %Lf %Lf %Lf %Lf %Lf\n" ,node[n].coordinate[0], node[n].coordinate[1], rho, u, v, p, Ma, s, T ) ;
        fprintf( out, "%.20f %.20f %.20f %.20f %.20f %.20f %.20f %.20f %.20f %.20f %.20f %.20f\n",node[n].coordinate[0], node[n].coordinate[1], rho, u, v, p, Ma, s, T ) ;
    }
  
    for ( e = 0 ; e < NE ; e++ ) fprintf( out, "%d %d %d \n", element[e].node[0] + 1,
                    element[e].node[1] + 1,
                    element[e].node[2] + 1 ) ;
    fclose( out ) ;

}
*/


/*********************************************************/
/*********************************************************/
/**        PARAVIEW  reader/writer                **/
/*********************************************************/
/*********************************************************/

void write_solution_ascii_paraview()
{
    char paraview_file_name[MAX_CHAR];
    
    int i, n, e, TOT_N, TOT_E, lev, NNN;
    
    double rho, u, v, p, Ma, s, T, en, h1, h2, xc, yc, omega, mm, r, pm, dp, h0 , H;
    
    lev   = 2 ;
    TOT_N = NN ;
    TOT_E = NE ;

    printf( "          **********************************************\n" ) ;
    printf( "          **     Writing vtu file for paraview.....   **\n" ) ;
    printf( "          **********************************************\n" ) ;
    printf( "\n"                                                ) ;
    
    if ( movie ) {
        if (time_step < 100000) {
          sprintf( paraview_file_name, "%s0%d.vtu", out_file, time_step ) ;
        }
        if (time_step < 10000) {
          sprintf( paraview_file_name, "%s00%d.vtu", out_file, time_step ) ;
        }
        if (time_step < 1000) {
          sprintf( paraview_file_name, "%s000%d.vtu", out_file, time_step ) ;
        }
        if (time_step < 100) {
          sprintf( paraview_file_name, "%s0000%d.vtu", out_file, time_step ) ;
        }
        if (time_step < 10) {
          sprintf( paraview_file_name, "%s00000%d.vtu", out_file, time_step ) ;
        }
    }
    else {
        sprintf( paraview_file_name, "%s.vtu", out_file ) ;
    }
    
    printf( "\nWrite file %s\n\n", out_file);
    
    /*************************************************
     ***
     ***  WRITING CHECK file for debug - START
     ***
     ***************************************************/
    
    FILE *fp;
    fp=fopen("check.vtu", "w");
    
    /* ----------- file header ----------- */
    fprintf(fp, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\"  byte_order=\"BigEndian\">"
            "<UnstructuredGrid><Piece NumberOfPoints= \"%i\" NumberOfCells=\"%i\">\n", TOT_N, TOT_E );
    
    /* ----------- writing of points ----------- */
    fprintf( fp, "<Points><DataArray type = \"Float64\" NumberOfComponents=\"3\"  format=\"ascii\">\n");
    for ( i = 0; i < TOT_N; i++ ) {
        fprintf( fp, "%le %le %le\n", node[i].coordinate[0],node[i].coordinate[1], 0.);
    }
    fprintf( fp, "</DataArray></Points>\n");
    
    /* ----------- array of connected points ----------- */
    fprintf( fp, "<Cells><DataArray type=\"Int64\" Name=\"connectivity\" format=\"ascii\">\n");
    for ( e = 0; e < TOT_E; e++ ) {
        fprintf( fp, "%d %d %d\n", element[e].node[0], element[e].node[1], element[e].node[2] );
    }
    fprintf( fp, "</DataArray>\n" );
    
    /* ----------- offsets ----------- */
    fprintf( fp, "<DataArray type=\"Int64\" Name=\"offsets\" format=\"ascii\">\n" );
    for ( e = 0; e < NE; e++ ) {
        fprintf( fp, "%i\n", 3*(e+1) );
    }
    fprintf( fp, "</DataArray>\n" );
    
    /* ----------- variables ----------- */
    fprintf( fp, "<DataArray type=\"Int64\" Name=\"types\" format=\"ascii\">\n" );
    for ( e = 0; e < TOT_E; e++ ) {
        fprintf( fp, "5\n" );
    }
    fprintf( fp, "</DataArray></Cells>" );
    
    fprintf( fp, "<PointData>\n");
           
    /* ----------- U0 ----------- */
    fprintf( fp, "<DataArray type=\"Float64\" Name=\"U0\" format=\"ascii\">\n" );
    for ( i = 0; i < TOT_N; i++ ) {
        fprintf( fp, "%le\n", node[i].P[2][0] );
    }
    fprintf( fp, "</DataArray>\n");
    
    /* ----------- U1 ----------- */
           
    fprintf( fp, "<DataArray type=\"Float64\" Name=\"U1\" format=\"ascii\">\n" );
    for ( i = 0; i < TOT_N; i++ ) {
        fprintf( fp, "%le\n", node[i].P[2][1] );
    }
    fprintf( fp, "</DataArray>\n");
    
    /* ----------- U2 ----------- */
           
    fprintf( fp, "<DataArray type=\"Float64\" Name=\"U2\" format=\"ascii\">\n" );
    for ( i = 0; i < TOT_N; i++ ) {
        fprintf( fp, "%le\n", node[i].P[2][2] );
    }
    fprintf( fp, "</DataArray>\n");
    
    /* ----------- U3 ----------- */
           
    fprintf( fp, "<DataArray type=\"Float64\" Name=\"U3\" format=\"ascii\">\n" );
    for ( i = 0; i < TOT_N; i++ ) {
        fprintf( fp, "%le\n", node[i].P[2][3] );
    }
    fprintf( fp, "</DataArray>\n");
    
    fprintf( fp, "</PointData>\n");
    fprintf( fp, "</Piece></UnstructuredGrid></VTKFile>\n" );
    
    /*************************************************
    ***
    ***  WRITING CHECK file for debug - END
    ***
    ***************************************************/
    
    fclose( fp );
    
    /*************************************************
    ***
    ***  WRITING SOLUTION - START
    ***
    ***************************************************/
    
    out = fopen( paraview_file_name, "w" ) ;

    /* ----------- file header ----------- */
    fprintf(out, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\"  byte_order=\"BigEndian\">"
            "<UnstructuredGrid><Piece NumberOfPoints= \"%i\" NumberOfCells=\"%i\">\n", TOT_N, TOT_E );
    
    /* ----------- writing of points ----------- */
    fprintf(out, "<Points><DataArray type = \"Float64\" NumberOfComponents=\"3\"  format=\"ascii\">\n");
    for ( i = 0; i < TOT_N; i++ ) {
        fprintf(out, "%le %le %le\n", node[i].coordinate[0],node[i].coordinate[1], 0.);
    }
    fprintf(out, "</DataArray></Points>\n");
    
    /* ----------- array of connected points ----------- */
    fprintf(out, "<Cells><DataArray type=\"Int64\" Name=\"connectivity\" format=\"ascii\">\n");
    for ( e = 0; e < TOT_E; e++ ) {
        fprintf(out, "%d %d %d\n", element[e].node[0], element[e].node[1], element[e].node[2] );
    }
    fprintf(out, "</DataArray>\n" );
    
    /* ----------- offsets ----------- */
    fprintf(out, "<DataArray type=\"Int64\" Name=\"offsets\" format=\"ascii\">\n" );
    for ( e = 0; e < TOT_E; e++ ) {
        fprintf( fp, "%i\n", 3*(e+1) );
    }
    fprintf(out, "</DataArray>\n" );
    
    /* ----------- variables ----------- */
    fprintf( out, "<DataArray type=\"Int64\" Name=\"types\" format=\"ascii\">\n" );
    for ( e = 0; e < TOT_E; e++ ) {
        fprintf(out, "5\n" );
    }
    fprintf(out, "</DataArray></Cells>" );
    
    fprintf( out, "<PointData>\n");
    
    /* ----------- rho ----------- */
    fprintf( out, "<DataArray type=\"Float64\" Name=\"rho\" format=\"ascii\">\n" );
    for ( i = 0; i < TOT_N; i++ ) {
        fprintf( out, "%le\n", node[i].P[2][0] );
    }
    fprintf( out, "</DataArray>\n");
    
    /* ----------- u ----------- */
    fprintf( out, "<DataArray type=\"Float64\" Name=\"u\" format=\"ascii\">\n" );
    for ( i = 0; i < TOT_N; i++ ) {
        fprintf( out, "%le\n", node[i].Z[2][1] );
    }
    fprintf( out, "</DataArray>\n");
    
    /* ----------- v ----------- */
    fprintf( out, "<DataArray type=\"Float64\" Name=\"v\" format=\"ascii\">\n" );
    for ( i = 0; i < TOT_N; i++ ) {
        fprintf( out, "%le\n", node[i].Z[2][2] );
    }
    fprintf( out, "</DataArray>\n");
    
    /* ----------- p ----------- */
    fprintf( out, "<DataArray type=\"Float64\" Name=\"p\" format=\"ascii\">\n" );
    for ( i = 0; i < TOT_N; i++ ) {
        fprintf( out, "%le\n", node[i].Z[2][3] );
    }
    fprintf( out, "</DataArray>\n");
    
    /* ----------- H ----------- */
    fprintf( out, "<DataArray type=\"Float64\" Name=\"H\" format=\"ascii\">\n" );
    for ( i = 0; i < TOT_N; i++ ) {
        H = gm*node[i].Z[2][3]/( (gm-1.0)*node[i].Z[2][0] )
            + 0.5*( node[i].Z[2][1]*node[i].Z[2][1] + node[i].Z[2][2]*node[i].Z[2][2] )  ;
        fprintf( out, "%le\n", H );
    }
    fprintf( out, "</DataArray>\n");
    
    /* ----------- Ma ----------- */
    fprintf( out, "<DataArray type=\"Float64\" Name=\"Ma\" format=\"ascii\">\n" );
    for ( i = 0; i < TOT_N; i++ ) {
        Ma = node[i].Z[2][0]*( node[i].Z[2][1]*node[i].Z[2][1] + node[i].Z[2][2]*node[i].Z[2][2] )/( gm*node[i].Z[2][3] ) ;
        Ma = sqrt( Ma ) ;
        fprintf( out, "%le\n", Ma );
    }
    fprintf( out, "</DataArray>\n");
    
    /* ----------- s ----------- */
    fprintf( out, "<DataArray type=\"Float64\" Name=\"Ma\" format=\"ascii\">\n" );
    for ( i = 0; i < TOT_N; i++ ) {
        s = node[i].Z[2][3]/pow( node[i].Z[2][0], gm ) ;
        fprintf( out, "%le\n", s );
    }
    fprintf( out, "</DataArray>\n");
    
    /* ----------- e ----------- */
    fprintf( out, "<DataArray type=\"Float64\" Name=\"e\" format=\"ascii\">\n" );
    for ( i = 0; i < TOT_N; i++ ) {
        T = node[n].P[2][3]/node[n].P[2][0] - 0.5*( node[i].Z[2][1]*node[i].Z[2][1] + node[i].Z[2][2]*node[i].Z[2][2] ) ;
        fprintf( out, "%le\n", T );
    }
    fprintf( out, "</DataArray>\n");
    
    /* ----------- time ----------- */

    fprintf( out, "<DataArray type=\"Float64\" Name=\"e\" format=\"ascii\">\n" );
    for ( i = 0; i < TOT_N; i++ ) {
        fprintf( out, "%le\n", time );
    }
    fprintf( out, "</DataArray>\n");
    
    /* ----------- shock sensor ----------- */

    fprintf( out, "<DataArray type=\"Float64\" Name=\"sensor\" format=\"ascii\">\n" );
    for ( i = 0; i < TOT_N; i++ ) {
        fprintf( out, "%le\n", node[i].tt );
    }
    
    fprintf( out, "</PointData>\n");
    fprintf( out, "</Piece></UnstructuredGrid></VTKFile>\n" );
    
    /*************************************************
    ***
    ***  WRITING SOLUTION - END
    ***
    ***************************************************/
    
    fclose( out );
    
    if ( initial_state == 9 ) {
        h0 = h1 = h2 = 0. ;
        for ( n = 0 ; n < TOT_N ; n++ ) {
            xc    = node[n].coordinate[0] - 0.5 ;
            yc    = node[n].coordinate[1] - 0.5 ;
            r     = sqrt( xc*xc + yc*yc ) ;
            
            pm    = 100. ;
            mm = p - pm ;
            dp = 0. ;
            
            if ( r <= 0.25 ) {
                omega = 4.*pi*r ;
                omega = 15.*( 1. + cos( omega ) ) ;
                r     = 4.*pi*r ;
                dp    = ( 225.*1.4/( 16.*pi*pi ) )*( 2.*cos( r ) + 2.*r*sin( r ) + cos( 2.*r )/8. + r*sin( 2.*r )/4. + r*r*12./16. ) ;
                dp   -= ( 225.*1.4/( 16.*pi*pi ) )*( 2.*cos( pi ) + 2.*pi*sin( pi ) + cos( 2.*pi )/8. + pi*sin( 2.*pi )/4. + pi*pi*12./16. ) ;
            }
            
            mm -= dp ;
            mm = fabs( mm )/pm ;
            
            if ( mm > h0 ) {
                h0 = mm  ;
            }
            
            h1 += mm/(1.*TOT_N) ;
            h2 += mm*mm/(1.*TOT_N) ;
        }
        
        h2 = sqrt( h2 ) ;
        
        printf("Errors\n") ;
        printf("Infty: %le\n",log10(h0)) ;
        printf("one: %le\n",log10(h1)) ;
        printf("two: %le\n",log10(h2)) ;
        printf("1/40: %le\n",log(1./40.)) ;
        printf("1/80: %le\n",log(1./80.)) ;
        printf("1/160: %le\n",log(1./160.)) ;
        printf("1/200: %le\n",log(1./200.)) ;
        printf("Ntot:%d\n",TOT_N) ;
    }
      
}

void read_solution_ascii_paraview()
{
    char init_file_name[MAX_CHAR] ;
    int i,  e, d1, d2, d3, TOT_N, TOT_E, NNN, ret ;
    double rho, u, v, p, d4, d5, d6 ;
     
    TOT_N = NN;
    TOT_E = NE;
    
    printf( "          *************************************\n" ) ;
    printf( "          **     Reading Paraview file....   **\n" ) ;
    printf( "          *************************************\n" ) ;
    printf( "\n" ) ;
    
    sprintf( init_file_name, "%s.vtu", initial_solution_file ) ;
    
    initial = fopen( init_file_name, "r" ) ;
    
    ret = fscanf(initial, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\"  byte_order=\"BigEndian\">"
           "<UnstructuredGrid><Piece NumberOfPoints= \"%i\" NumberOfCells=\"%i\">\n", &d1, &d2 );
    
    /* ----------- points ----------- */
    ret = fscanf( initial, "<Points><DataArray type = \"Float64\" NumberOfComponents=\"3\"  format=\"ascii\">\n");
    for ( i = 0; i < TOT_N; i++ ) {
        ret = fscanf(initial, "%le %le %le\n", &d4, &d5, &d6) ;
    }
    ret = fscanf( initial, "</DataArray></Points>\n");
    
    /* ----------- connectivity ----------- */
    ret = fscanf( initial, "<Cells><DataArray type=\"Int64\" Name=\"connectivity\" format=\"ascii\">\n");
    
    for ( e = 0; e < TOT_E; e++ ) {
        ret = fscanf( initial, "%d %d %d\n", &d1, &d2, &d3 );
    }
    ret = fscanf( initial, "</DataArray>\n" );
    
    /* ----------- offsets ----------- */
    ret = fscanf( initial, "<DataArray type=\"Int64\" Name=\"offsets\" format=\"ascii\">\n" );
    
    for ( e = 0; e < TOT_E; e++ ) {
        ret = fscanf( initial, "%i\n", &d1 );
    }
    ret = fscanf( initial, "</DataArray>\n" );
    
    /* ----------- types ----------- */
    ret = fscanf( initial, "<DataArray type=\"Int64\" Name=\"types\" format=\"ascii\">\n" );
    
    for ( e = 0; e < TOT_E; e++ ) {
        ret = fscanf( initial, "5\n" );
    }
    ret = fscanf( initial, "</DataArray></Cells>" );
    
    ret = fscanf( initial, "<PointData>\n");
    
    /* ----------- rho ----------- */
    ret = fscanf( initial, "<DataArray type=\"Float64\" Name=\"rho\" format=\"ascii\">\n" );
    for ( i = 0; i < TOT_N; i++ ) {
        ret = fscanf( initial, "%le\n", &node[i].Z[2][0] );
    }
    ret = fscanf( initial, "</DataArray>\n");
    
    /* ----------- u ----------- */
    ret = fscanf( initial, "<DataArray type=\"Float64\" Name=\"u\" format=\"ascii\">\n" );
    for ( i = 0; i < TOT_N; i++ ) {
        ret = fscanf( initial, "%le\n", &node[i].Z[2][1] );
    }
    ret = fscanf( initial, "</DataArray>\n");
    
    /* ----------- v ----------- */
    ret = fscanf( initial, "<DataArray type=\"Float64\" Name=\"v\" format=\"ascii\">\n" );
    for ( i = 0; i < TOT_N; i++ ) {
        ret = fscanf( initial, "%le\n", &node[i].Z[2][2] );
    }
    ret = fscanf( initial, "</DataArray>\n");
           
    /* ----------- p ----------- */
    ret = fscanf( initial, "<DataArray type=\"Float64\" Name=\"p\" format=\"ascii\">\n" );
    for ( i = 0; i < TOT_N; i++ ) {
        ret = fscanf( initial, "%le\n", &node[i].Z[2][3] );
    }
    ret = fscanf( initial, "</DataArray>\n");
    
    fclose( initial ) ;
    
    for ( i = 0; i < TOT_N; i++ ) {
        Z_2_P( node[i].Z[2], node[i].P[2] ) ;
    }
}




/*********************************************************/
/*********************************************************/
/**           LibMeshb  reader/writer                   **/
/*********************************************************/
/*********************************************************/

void read_exact_solution_meshb()
{
    char       init_file_path[MAX_CHAR], fileName[MAX_CHAR];
    int        gmfVersion, dim, numSolAtVerticesLines, numSolTypes, solSize, solTypesTable[GmfMaxTyp];
    long long  solIndex;
    double     buffer[4];

    sprintf(init_file_path, "%s.exact", initial_solution_file ) ;
    strcpy(fileName, init_file_path);
    strcat(fileName, ".solb");
    if ( !(solIndex = GmfOpenMesh(fileName, GmfRead, &gmfVersion, &dim)) ) {
        strcpy(fileName, init_file_path);
        strcat(fileName,".sol");
        if ( !(solIndex = GmfOpenMesh(fileName, GmfRead, &gmfVersion, &dim)) ) {
            printf("####  ERROR Mesh file %s.sol[b] not found ", init_file_path);
            exit(1);
        }    
    }
    if (dim != 2) {
        printf("####  ERROR  Wrong dimension: %d != 2\n", dim);
        exit(1);
    }

    numSolAtVerticesLines = GmfStatKwd(solIndex, GmfSolAtVertices, &numSolTypes, &solSize, solTypesTable);  
    if( numSolAtVerticesLines == 0 ) {
        printf("####  ERROR  No SolAtVertices in the solution file %s\n", fileName);
        exit(1);
    }
    else if (numSolAtVerticesLines != NN) {
        printf("####  ERROR  The number of solution lines is different from the number of mesh vertices: %d != %d\n", numSolAtVerticesLines, NN);
        exit(1);
    }
  
    if (numSolTypes != 3) {
        printf("####  ERROR  Wrong number of solution fields in %s. Expecting 3 and got %d\n", fileName, numSolTypes);
        exit(1);
    }
    if (solTypesTable[0] != GmfSca && solTypesTable[1] != GmfVec && solTypesTable[2] != GmfSca) {
        printf("####  ERROR  Wrong solution types in %s. Expecting scalar, vector, scalar\n", fileName);
        exit(1);
    }

    GmfGotoKwd(solIndex, GmfSolAtVertices);
    for (int n = 0; n < NN; ++n) {
        GmfGetLin(solIndex, GmfSolAtVertices, buffer);
        node[n].exact_sol[0] = buffer[0];
        node[n].exact_sol[1] = buffer[1];
        node[n].exact_sol[2] = buffer[2];
        node[n].exact_sol[3] = buffer[3];
    }

    GmfCloseMesh(solIndex);
    
    for (int n = 0; n < NN; n++) {
        Z_2_P(node[n].Z[2], node[n].P[2]) ;
    }
}


void read_solution_meshb()
{
    char       init_file_path[MAX_CHAR], fileName[MAX_CHAR];
    int        gmfVersion, dim, numSolAtVerticesLines, numSolTypes, solSize, solTypesTable[GmfMaxTyp];
    long long  solIndex;
    double     buffer[4];

    sprintf(init_file_path, "%s", initial_solution_file ) ;
    strcpy(fileName, init_file_path);
    strcat(fileName, ".solb");
    if ( !(solIndex = GmfOpenMesh(fileName, GmfRead, &gmfVersion, &dim)) ) {
        strcpy(fileName, init_file_path);
        strcat(fileName,".sol");
        if ( !(solIndex = GmfOpenMesh(fileName, GmfRead, &gmfVersion, &dim)) ) {
            printf("####  ERROR Mesh file %s.sol[b] not found ", init_file_path);
            exit(1);
        }    
    }
    if (dim != 2) {
        printf("####  ERROR  Wrong dimension: %d != 2\n", dim);
        exit(1);
    }

    numSolAtVerticesLines = GmfStatKwd(solIndex, GmfSolAtVertices, &numSolTypes, &solSize, solTypesTable);  
    if( numSolAtVerticesLines == 0 ) {
        printf("####  ERROR  No SolAtVertices in the solution file %s\n", fileName);
        exit(1);
    }
    else if (numSolAtVerticesLines != NN) {
        printf("####  ERROR  The number of solution lines is different from the number of mesh vertices: %d != %d\n", numSolAtVerticesLines, NN);
        exit(1);
    }
  
    if (numSolTypes < 3) {
        printf("####  ERROR  Wrong number of solution fields in %s. Expecting 3 and got %d\n", fileName, numSolTypes);
        exit(1);
    }
    if (solTypesTable[0] != GmfSca && solTypesTable[1] != GmfVec && solTypesTable[2] != GmfSca) {
        printf("####  ERROR  Wrong solution types in %s. Expecting scalar, vector, scalar\n", fileName);
        exit(1);
    }

    GmfGotoKwd(solIndex, GmfSolAtVertices);
    for (int n = 0; n < NN; ++n) {
        GmfGetLin(solIndex, GmfSolAtVertices, buffer);
        node[n].Z[2][0] = buffer[0];
        node[n].Z[2][1] = buffer[1];
        node[n].Z[2][2] = buffer[2];
        node[n].Z[2][3] = buffer[3];
    }

    GmfCloseMesh(solIndex);
    
    for (int n = 0; n < NN; n++) {
        Z_2_P(node[n].Z[2], node[n].P[2]) ;
    }
}


void write_solution_meshb()
{
    char      out_file_path[MAX_CHAR], fileName[MAX_CHAR];
    long long meshIndex, solIndex;
    int       solKeyword;
    double    buffer[10], H, Ma, s, T;

    int ascii       = 0;
    int dim         = 2;
    int fileVersion = GmfDouble;
    int solNumber   = 9;
    int solTypes[9] = {GmfSca, GmfVec, GmfSca, GmfSca, GmfSca, GmfSca, GmfSca, GmfSca, GmfSca};



    solKeyword = GmfSolAtVertices;
    if (movie) {
        //sprintf(out_file_path, "%s.%06d", out_file, time_step ) ;
        sprintf(out_file_path, "%s.%d", out_file, (int) (time_step/save_solution) ) ;
    } 
    else {
        sprintf(out_file_path, "%s", out_file ) ;
    }
    strcpy(fileName, out_file_path);
    if ( ascii ) {
        strcat(fileName, ".sol");
    }
    else {
        strcat(fileName, ".solb");    
    }
         
    if ( !(solIndex = GmfOpenMesh(fileName, GmfWrite, fileVersion, dim)) ) {
        fprintf(stderr,"####  ERROR: solution file %s cannot be opened\n", fileName);
        exit(1);
    }

    GmfSetKwd(solIndex, solKeyword, NN, solNumber, solTypes);

    for (int n = 0; n < NN; ++n) {
        buffer[0] = node[n].Z[2][0];
        buffer[1] = node[n].Z[2][1];
        buffer[2] = node[n].Z[2][2];
        buffer[3] = node[n].Z[2][3];
        H = gm*node[n].Z[2][3]/((gm-1.0)*node[n].Z[2][0])
          + 0.5*(node[n].Z[2][1]*node[n].Z[2][1] + node[n].Z[2][2]*node[n].Z[2][2]);
        buffer[4] = H;
        Ma = node[n].Z[2][0]*(node[n].Z[2][1]*node[n].Z[2][1] + node[n].Z[2][2]*node[n].Z[2][2])/(gm*node[n].Z[2][3]);
        Ma = sqrt(Ma) ;
        buffer[5] = Ma;
        s = node[n].Z[2][3]/pow( node[n].Z[2][0], gm);
        buffer[6] = s;
        T = node[n].P[2][3]/node[n].P[2][0] - 0.5*(node[n].Z[2][1]*node[n].Z[2][1] + node[n].Z[2][2]*node[n].Z[2][2]);
        buffer[7] = T;
        buffer[8] = time;
        buffer[9] = node[n].tt;

        GmfSetLin(solIndex, solKeyword, buffer);
    }

    GmfCloseMesh(solIndex);
}

/***************************************************************************
 *                                                                         *
 *   This program has been developed at the von Karman Institute for Fluid *
 *   Dynamics by Mario Ricchiuto. Any change or update must be done in a   *
 *   clean way and it must be clearly commented.                           *
 *   Mario Ricchiuto                                                       *
 *                                              23/04/2002                 *
 ***************************************************************************/
