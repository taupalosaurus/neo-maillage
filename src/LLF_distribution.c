/***************************************************************************
 N_distribution.c
 ----------------
 This is N_distribution: here the distribution of the cell fluctuation is
 performed through the N scheme distribution function KP(U_i-U_c)
 -------------------
 begin                : Tue May 14 2002
 copyright            : (C) 2002 by Mario Ricchiuto
 email                : ricchiut@vki.ac.be
 ***************************************************************************/
#include "common.h"

extern struct element_struct *element ;
extern struct node_struct *node ;

extern double *work_vector, *work_vector0, *work_vector1, *work_vector2 ;
extern double **PHI_d, **PHIN_d, *phi_a_w0, *phi_a_w1, *phi_w, **PHIN_scal ;
extern double ***K_i_p1, ***K_i_p0, **sum_K, **sum_K_1, ***K1 ;
extern double *temp_vector, *normal, *vel, *phi_node, **Left, **Right, **temp_mat ;
extern double **W,  **Z, *U_C, dt, **phi_t_w, alpha, **dU  ;

extern double diag0, diag1, q_ratio, ubar0, vbar0, speed_of_sound0, shield_factor ;

extern int size, lump_type , iteration ;

extern void velocity() ;
extern void initmat( double ** ) ;
extern void add2mat( double **, double ** ) ;
extern void initvec( double * ) ;
extern void add2vec( double *, double * ) ;
extern void A_times_v( double **, double *, double * ) ;
extern void residual_update( int ) ; /* node global index, local residual distributed to node, time level to update (1/2) */
extern void invertmat( double **, double ** ) ;
extern void decompose( double *, double *, double * ) ;
/* direction for the decomposition, nodal N scheme residual, scalar residuals */
extern void limiter( double *, double **, double *, int, int ) ;
/* scalar components of the cell fluctuation, N scheme scalar residuals, limited scalar nodal residuals, target node, nodes involved in the limiting */
extern void recompose( double *, double *, double * ) ;
/* direction for the decomposition, scalar P scheme residuals, P scheme residual */

extern void Eigenvectors( double * ) ;

void LLF_unsteady( int e )
{
    int j ,v, n1, k, l ;
    double volume, betaP, arg, r_max, u_max, nx, ny ;
    double length_sq, theta, a, b, c, tau ;
    double DDV[3][4], diss[3][4], lump, mariuz, Deltat[3] ;
    double p_max, p_min, coeff ;
    double div, vel_x, vel_y, ale_corr_fact ;
    
    lump = 1.0*lump_type ;
    
    volume = element[e].volume/3.0 ;
    
    //length = dt*element[e].volume ;
    //length = pow( length, 1./3. ) ;
    
    length_sq = volume ;
    
    initmat( sum_K ) ; // for Bc
    
    //
    // computing the divergence div(sigma)
    //
    div = 0. ;
    for ( v = 0 ; v < 3 ; v ++ )
    {
        Deltat[v] = node[element[e].node[v]].dtau;
        
        nx = element[e].normal[v][0] ;
        ny = element[e].normal[v][1] ;
        
        if  ( nx*nx + ny*ny > length_sq )
            length_sq = nx*nx + ny*ny ;
        
        vel_x = node[element[e].node[v]].vel[0] ;
        vel_y = node[element[e].node[v]].vel[1] ;
        
        div += 0.5* ( vel_x*nx + vel_y*ny ) ;
        
    }
    
    for( v = 0 ; v < 3 ; v++ )
        add2mat( sum_K, K_i_p1[v] ) ; // for Bc
    
    invertmat( sum_K, sum_K_1 ) ; //for Bc
    
    
    
    for ( j = 0 ; j < size ; j ++ )
    {
        // LF dissipation
        diss[0][j]  = 2.*W[0][j] - W[1][j] - W[2][j] ; diss[0][j] *= alpha/3. ;
        diss[1][j]  = 2.*W[1][j] - W[2][j] - W[0][j] ; diss[1][j] *= alpha/3. ;
        diss[2][j]  = 2.*W[2][j] - W[0][j] - W[1][j] ; diss[2][j] *= alpha/3. ;
        //
        // ALE Mass matrix corrections
        //
        
        ale_corr_fact = ( 1. + dt/(2.*element[e].volume)*div ) ;
        
        DDV[0][j] = ale_corr_fact* ( -( 1. - lump )*( 2.*dU[0][j] + dU[1][j] + dU[2][j] )/4. - lump*dU[0][j] ) ;
        DDV[1][j] = ale_corr_fact* ( -( 1. - lump )*( 2.*dU[1][j] + dU[0][j] + dU[2][j] )/4. - lump*dU[1][j] ) ;
        DDV[2][j] = ale_corr_fact* ( -( 1. - lump )*( 2.*dU[2][j] + dU[1][j] + dU[0][j] )/4. - lump*dU[2][j] ) ;
    }
    
    for ( j = 0 ; j < size ; j ++ )
    {
        //LF scheme
        PHIN_d[0][j] = dU[0][j]/Deltat[0] + phi_a_w1[j]/3. + diss[0][j] ;
        PHIN_d[1][j] = dU[1][j]/Deltat[1] + phi_a_w1[j]/3. + diss[1][j] ;
        PHIN_d[2][j] = dU[2][j]/Deltat[2] + phi_a_w1[j]/3. + diss[2][j] ;
        // Tau matrix
        for( l = 0 ; l < size ; l++ )
        {
            for( v = 0 ; v < 3 ; v++ )
            {
                K_i_p0[v][j][l] = 0.0 ;
                //                     K_i_p0[v][j][l] = 0.5*K1[v][j][l]/alpha ;
                for ( k = 0 ; k < size ; k++ )
                    K_i_p0[v][j][l] += 0.5*sum_K_1[j][k]*K1[v][k][l] ;
            }
        }
    }
    
    
    
    
    velocity() ;
    
    Eigenvectors( vel ) ;
    
    /* limiting and redistribution of the scalar residuals */
    
    
    for ( k = 0 ; k < size ; k ++ )
    {
        work_vector0[k] = 0. ;
        PHIN_scal[0][k] = 0. ;
        PHIN_scal[1][k] = 0. ;
        PHIN_scal[2][k] = 0. ;
        
        for ( l = 0; l < size ; l ++ )
        {
            work_vector0[k] += Left[k][l]*phi_w[l] ;
            PHIN_scal[0][k] += Left[k][l]*PHIN_d[0][l] ;
            PHIN_scal[1][k] += Left[k][l]*PHIN_d[1][l] ;
            PHIN_scal[2][k] += Left[k][l]*PHIN_d[2][l] ;
        }
    }
    
    theta = length_sq/( fabs( work_vector0[0] )  + 1.e-15 ) ;
    
    if ( theta >= 1. )
        theta = 1.0 ;
    
    mariuz = 1.e-15 ;
    
    for ( v = 0 ; v < 3 ; v ++ )
        node[element[e].node[v]].tt +=  theta*volume/node[element[e].node[v]].vol ;
    
    
    for ( v = 0 ; v < 3 ; v ++ )
    {
        for ( j = 0 ; j < size ; j ++ )
        {
            if ( fabs( work_vector0[j] ) > 1.e-20 )
            {
                arg = PHIN_scal[0][j]/work_vector0[j] ;
                if ( arg > 0. ) betaP  = arg + mariuz ;
                else betaP = mariuz ;
                
                arg = PHIN_scal[1][j]/work_vector0[j] ;
                if ( arg > 0. ) betaP  += arg + mariuz ;
                else betaP += mariuz ;
                
                arg = PHIN_scal[2][j]/work_vector0[j] ;
                if ( arg > 0. ) betaP  += arg + mariuz ;
                else betaP += mariuz ;
                
                arg    = PHIN_scal[v][j]/work_vector0[j] ;
                if ( arg > 0. ) betaP  = ( arg + mariuz )/betaP ;
                else betaP = mariuz/betaP ;
                
                work_vector1[j] = betaP*work_vector0[j] ;
            }
            else work_vector1[j] = 0. ;
        }
        
        for ( k = 0 ; k < size ; k ++ )
        {
            //                  phi_node[k] = DDV[v][k]  + theta*phi_w[k]/3. ; // for Bc
            phi_node[k] = DDV[v][k]/Deltat[v] ; // for LLF
            
            // Projecting back + dissipation......
            for ( l = 0 ; l < size ; l ++ )
                //                      phi_node[k] += ( 1. - theta )*Right[k][l]*work_vector1[l] + theta*K_i_p0[v][k][l]*phi_w[l] ; // for Bc
                phi_node[k] += Right[k][l]*work_vector1[l] + theta*K_i_p0[v][k][l]*phi_w[l] ; // for LLf
        }
        
        residual_update( element[e].node[v] ) ;
    }
}


/***************************************************************************
 *                                                                         *
 *   This program has been developed at the von Karman Institute for Fluid *
 *   Dynamics by Mario Ricchiuto. Any change or update must be done in a   *
 *   clean way and it must be clearly commented.                           *
 *   Mario Ricchiuto                                                       *
 *                                              23/04/2002                 *
 ***************************************************************************/
