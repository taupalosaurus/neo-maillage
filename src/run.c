/***************************************************************************
 run.c
 -----
 This is run: it drives the real computation
 -------------------
 begin                : Mon May 6 2002
 copyright            : (C) 2002 by Mario Ricchiuto
 email                : ricchiut@vki.ac.be
 ***************************************************************************/
#include "common.h"

extern int initial_state, time_step, time_step_max, save_solution, info_freq, counter_info, steady_check  ;
extern int exact, exact_format, problem, error_freq;

extern double time, t_max, lim_norm, residual_norm, residual_treshold, ref_norm ;
extern double residual_norm, ref_norm, dt ;

extern void initialize_solution() ;
extern void compute_time_step() ;
extern void move_compute_save_the_mesh() ;
extern void pseudo_time_stepper() ;
extern void element_preprocessing() ;
extern void compute_error(double*);
extern void compute_error_quadrature(double*, int);

extern void (*write_solutions)() ;




void print_timestep_info()
{
    double errors[3], errors_nod[3];

    if ( exact && error_freq == 1) {
        if (exact_format == -1) {
            if (problem != 8 && problem != 0) {
                printf("### WARNING: for now, only the shockednozzle and rectangular reflexion analytic solutions are implemented\n");
            }
            compute_error_quadrature(errors, problem);
            compute_error(errors_nod);
        }
        else {
            compute_error(errors);
        }
    }

    printf( "\n" ) ;
    printf( "          *************************************\n" ) ;
    printf( "          *************************************\n" ) ;
    printf( "          **      timestep information:      **\n" ) ;
    printf( "          ** MAX number of iteration reached **\n" ) ;
    printf( "          *************************************\n" ) ;
    printf( "               LOG10( norm1_res ) = %le \n", residual_norm ) ;
    /*printf( "               Initial norm       = %le \n", ref_norm ) ;*/
    printf( "               Time step          = %le \n", dt ) ;
    if ( exact && error_freq == 1) {
        printf( "               Errors (L1,L2,Linf)  = %1.3e %1.3e %1.3e \n", errors[0],errors[1], errors[2] ) ;
        if (problem == 8 || problem == 0) {
            printf( "               Errors (l1,l2,linf)  = %1.3e %1.3e %1.3e \n", errors_nod[0],errors_nod[1], errors_nod[2] ) ;
        }
    }
    printf( "               Time Iteration = %d \n", time_step ) ;
    printf( "               Time Reached = %le \n", time ) ;
    printf( "\n" ) ;
            
    counter_info = 0 ;
}


void run()
{
    int    counter, steady_state ;
    double errors[3], errors_nod[3];
    
    //printf("maximum time %lf \n", t_max);
    printf( "\n" ) ;
    printf( "          *************************************\n" ) ;
    printf( "          *************************************\n" ) ;
    printf( "          **                                 **\n" ) ;
    printf( "          **             End of the          **\n" ) ;
    printf( "          **                                 **\n" ) ;
    printf( "          **        pre-processing phase     **\n" ) ;
    printf( "          **                                 **\n" ) ;
    printf( "          *************************************\n" ) ;
    printf( "          **                                 **\n" ) ;
    printf( "          ** The real computation starts.... **\n" ) ;
    printf( "          **                                 **\n" ) ;
    printf( "          *************************************\n" ) ;
    printf( "          *************************************\n" ) ;
    printf( "\n" ) ;
    //printf("maximum nof time steps %ld \n", time_step_max);
    
    counter_info = 0 ;
    time_step = 0 ;
    counter = 0 ;
    time = 0.0 ;
    steady_state = 0 ;

    print_timestep_info();
    
    while ( ( t_max > time + small  ) &&( time_step < time_step_max )  && ( steady_state < 1 ) ) {
        time_step++ ;
        counter ++ ;
        counter_info ++ ;
        
        initialize_solution() ;
        compute_time_step() ;
        move_compute_save_the_mesh() ;
        element_preprocessing() ;
        pseudo_time_stepper() ;
        
        if ( counter == info_freq) {
            print_timestep_info();
        }
        
        if ( counter == save_solution ) {
            counter = 0 ;
            //printf("Before write_solution\n");
            write_solutions() ;
        }
        
        if ( steady_check == 1 ) {
            if ( ( residual_norm > lim_norm ) && ( residual_norm > ref_norm +residual_treshold  ) ) {
                steady_state = 0 ;
            }
            else {
                steady_state = 1 ;
            }
        }
    }// end WHILE

    printf( "\n" ) ;
    printf( "          *************************************\n" ) ;
    printf( "          *************************************\n" ) ;
    printf( "          **                                 **\n" ) ;
    printf( "          **        run ended due to:        **\n" ) ;
    printf( "                                             \n" ) ;

    if ( residual_norm <= ref_norm +residual_treshold ) {
        printf( "             Steady state conditions reached \n" ) ;
        printf( "                residual norm - initial norm %le\n",residual_norm ) ;
        printf( "                    smaller than the    \n" ) ;
        printf( "                Relative Convergence limit %le\n", residual_treshold  ) ;
    }
    
    if ( residual_norm <= lim_norm ) {
        printf( "             Steady state conditions reached \n" ) ;
        printf( "                    residual norm %le\n",residual_norm ) ;
        printf( "                    smaller than the    \n" ) ;
        printf( "                    Convergence limit %le\n", lim_norm ) ;
    }

    if ( t_max < time ) {
        printf( "                      Final time  %le\n",t_max ) ;
        printf( "                    has been reached        \n" ) ;
    }
    if ( time_step >= time_step_max  ) {
        printf( "                      Max number of \n" ) ;
        printf( "                      time steps  %d\n",time_step_max ) ;
        printf( "                     has been reached        \n" ) ;
    }

    if ( exact && error_freq>0) {
        if (exact_format == -1) {
            if (problem != 6 && problem != 7 && problem != 8 && problem != 9) {
                printf("### WARNING: for now, only the shockednozzle and rectangular reflexion analytic solutions are implemented\n");
            }
            compute_error_quadrature(errors, problem);
            compute_error(errors_nod);
        }
        else {
            compute_error(errors);
        }
        printf( "             Errors (L1,L2,Linf)  = %1.3e %1.3e %1.3e \n", errors[0],errors[1], errors[2] ) ;
        if (problem == 8 || problem == 0) {
            printf( "             Errors (l1,l2,linf)  = %1.3e %1.3e %1.3e \n", errors_nod[0],errors_nod[1], errors_nod[2] ) ;
        }
    }
    
    printf( "                                            \n" ) ;
    printf( "          *************************************\n" ) ;
    printf( "          *************************************\n" ) ;
    printf( "\n" ) ;
    // printf("Should NOT be here ");
}



/***************************************************************************
 *                                                                         *
 *   This program has been developed at the von Karman Institute for Fluid *
 *   Dynamics by Mario Ricchiuto. Any change or update must be done in a   *
 *   clean way and it must be clearly commented.                           *
 *   Mario Ricchiuto                                                       *
 *                                              23/04/2002                 *
 ***************************************************************************/
