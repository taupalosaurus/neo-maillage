/***************************************************************************
                              input_parameters.c
                               ----------------
This is input_parameters: it reads and saves all the necessary informations
         needed to set up and run the computation  from the inputfile
                             --------------------
    begin                : Wed Apr 24 2002
    copyright            : (C) 2002 by Mario Ricchiuto
    email                : ricchiut@vki.ac.be
 ***************************************************************************/
#include "common.h"

extern int initial_state, scheme, local_timestepping ;
extern int iteration_max, convergence_variable, time_step_max, stop, movie, exact, info_freq, lump_type ;
extern int save_solution, out_format, in_format, mesh_format, exact_format, problem, error_freq, start_up, steady_check, out_lev ;

extern double shield_factor, CFL, residual_treshold, t_max, gm, lim_norm, ref_vel, zero_r, freezer ;

extern char initial_solution_file[MAX_CHAR] ;
extern char grid_file[MAX_CHAR] ;
extern char out_file[MAX_CHAR] ;


static void check_scanf(int ret, int expected_ret, int numline, char * line ) 
{
    if (ret != expected_ret) {
        printf("ERROR:  parser failed on line %d of the input file (expected ret: %d got %d\n) <%s>", numline, expected_ret, ret, line);
        exit(1);
    }

}


void input_parameters()
{
    int done, ret;
    FILE *inputfile ;

    inputfile = fopen( "inputfile-exp.txt", "r" ) ;
    if ( !inputfile ) printf( "ERROR: inputfile-exp.txt not found!\n" ) ;

    printf( "          *************************************\n" ) ;
    printf( "          **    Reading the inputfile.....   **\n" ) ;
    printf( "          *************************************\n" ) ;
    printf( "\n" ) ;

    char line[256];

    problem = 14; gm = 1.4; zero_r = 0.000001;
    scheme = 1; lump_type = 0; shield_factor = 1.2; freezer = 0; local_timestepping = 1;
    steady_check = 1; iteration_max = 1; convergence_variable = -1;
    stop = 0;
    mesh_format = 1; movie = 0; in_format = 1; out_format = 1; exact = 0;

    if (fgets(line, 256, inputfile) != NULL) {ret = sscanf( line, "   Initial state (0/1 - file/analytical)     %d    \n", &initial_state ); check_scanf(ret,1,11,line);}
    if (fgets(line, 256, inputfile) != NULL) {ret = sscanf( line, "   Initial solution file                     %s    \n", initial_solution_file ); check_scanf(ret,1,12,line);}
    if (fgets(line, 256, inputfile) != NULL) {ret = sscanf( line, "   External grid file                        %s    \n", grid_file ); check_scanf(ret,1,19,line);}
    if (fgets(line, 256, inputfile) != NULL) {ret = sscanf( line, "   Convergence threshold                     %le    \n", &residual_treshold ); check_scanf(ret,1,31,line);}
    if (fgets(line, 256, inputfile) != NULL) {ret = sscanf( line, "   Convergence limit                        %le    \n", &lim_norm ); check_scanf(ret,1,32,line);}
    if (fgets(line, 256, inputfile) != NULL) {ret = sscanf( line, "   Final time                               %le    \n", &t_max ); check_scanf(ret,1,39,line);}
    if (fgets(line, 256, inputfile) != NULL) {ret = sscanf( line, "   Maximum number of time steps              %d    \n", &time_step_max ); check_scanf(ret,1,40,line);}
    if (fgets(line, 256, inputfile) != NULL) {ret = sscanf( line, "   Steps to save                             %d    \n", &save_solution ); check_scanf(ret,1,47,line);}
    if (fgets(line, 256, inputfile) != NULL) {ret = sscanf( line, "   Information Frequency                     %d    \n", &info_freq ); check_scanf(ret,1,48,line);}
    if (fgets(line, 256, inputfile) != NULL) {ret = sscanf( line, "   Output file                               %s    \n", out_file ); check_scanf(ret,1,51,line);}
    

    fclose( inputfile ) ;
}

/***************************************************************************
 *                                                                         *
 *   This program has been developed at the von Karman Institute for Fluid *
 *   Dynamics by Mario Ricchiuto. Any change or update must be done in a   *
 *   clean way and it must be clearly commented.                           *
 *   Mario Ricchiuto                                                       *
 *                                              23/04/2002                 *
 ***************************************************************************/
