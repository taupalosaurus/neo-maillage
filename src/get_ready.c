/***************************************************************************
                                 get_ready.c
                                 ----------
        This is get_ready: anything that needs to be done to start up
                  the computation is done through get_ready.
                             -------------------
    begin                : Wed Apr 24 2002
    copyright            : (C) 2002 by Mario Ricchiuto
    email                : ricchiut@vki.ac.be
 ***************************************************************************/
#include "common.h"

extern void input_parameters() ;
extern void initialize() ;
extern void select_function() ;
extern void memory_allocation() ;
extern void geometry() ;
extern void initial_solution() ;
extern void write_shockednozzle();
extern void write_rectangularreflexion();
extern void test_error_quadrature();
extern void print_mesh();

extern void (*write_solutions)() ;
extern void (*read_exact_solutions)() ;
extern void ( *compute_exact_solution )() ;
extern int exact, exact_format, save_exact, problem;

void get_ready()
{
    printf( "\n" ) ;
    printf( "          *************************************\n" ) ;
    printf( "          *************************************\n" ) ;
    printf( "          **                                 **\n" ) ;
    printf( "          **      NEO has started .....      **\n" ) ;
    printf( "          **                                 **\n" ) ;
    printf( "          **       Sit down and relax        **\n" ) ;
    printf( "          **                                 **\n" ) ;
    printf( "          *************************************\n" ) ;
    printf( "          **                                 **\n" ) ;
    printf( "          **    Getting ready to compute...  **\n" ) ;
    printf( "          **                                 **\n" ) ;
    printf( "          *************************************\n" ) ;
    printf( "          *************************************\n" ) ;
    printf( "\n" ) ;

    input_parameters() ;

    select_function() ;

    initialize() ;

    memory_allocation() ;

    geometry() ;

    // test_error_quadrature();

    if (exact == -1) {
        compute_exact_solution();
    }
    if (save_exact == 1) {
        if (problem == 8) {
            write_shockednozzle();
        }
        else if (problem == 0) {
            write_rectangularreflexion();
        }
        else {
            printf("ERROR  No exact solution known for case %d\n", problem);
            exit(1);
        }
    }

    initial_solution() ;

    //print_mesh();

    write_solutions() ;

    if ( exact && exact_format != -1) {
        read_exact_solutions();
    }
}

/***************************************************************************
 *                                                                         *
 *   This program has been developed at the von Karman Institute for Fluid *
 *   Dynamics by Mario Ricchiuto. Any change or update must be done in a   *
 *   clean way and it must be clearly commented.                           *
 *   Mario Ricchiuto                                                       *
 *                                              23/04/2002                 *
 ***************************************************************************/
