#include "common.h"

extern struct node_struct *node ;
extern struct element_struct *element ;

extern int NN, NE;

extern void shockednozzle(double, double, double*, double*, double*);
extern void rectangular_reflexion(double, double, double*, double*, double*);


/*****************************************/
/**    Compute error wrt exact sol      **/
/*****************************************/

void compute_error(double * errors, int cas) 
{

    double errL1, errL2, errLinf, diff;

    errL1 = errL2 = errLinf = 0;
    for ( int n = 0 ; n < NN ; n++ ) {
        diff = fabs( node[n].Z[2][0] - node[n].exact_sol[0]);
        errL1 += diff;
        errL2 += diff*diff;
        errLinf = fmax(diff, errLinf);
    }
    errors[0] = errL1 / NN;
    errors[1] = sqrt(errL2 / NN);
    errors[2] = errLinf;
}


/********************************************************************/
/**  Compute error wrt exact sol with order 3 gaussian quadrature  **/
/********************************************************************/

void compute_error_quadrature(double * errors, int cas)
{
    double errL1, errL2, errLinf, diff;
    double x, y, r[3], r2[3], rho, vel[2], p, area;
    int idx_node;

    double wi[4] = {-0.5625 , 0.520833, 0.520833, 0.520833};
    double lbd[12] = {1./3, 1./3, 1./3, 0.6, 0.2, 0.2, 0.2, 0.6, 0.2, 0.2, 0.2, 0.6};

    errL1 = errL2 = errLinf = 0;
    for (int e=0; e<NE; ++e) {
        if (cas == 8) {
            // skip triangles accross the shock
            for (int i=0; i<3; ++i) {
                idx_node = element[e].node[i];
                x = node[idx_node].coordinate[0];
                y = node[idx_node].coordinate[1];
                r[i] = x*x + y*y - 1.5*1.5;
            }
            if (r[0]*r[1] <= 0 || r[0]*r[2] <= 0) {
                continue;
            }
        }
        else if (cas == 0) {
            // skip triangles accross the shock
            for (int i=0; i<3; ++i) {
                idx_node = element[e].node[i];
                x = node[idx_node].coordinate[0];
                y = node[idx_node].coordinate[1];
                r[i] = y + 1./1.2*x - 1;
                r2[i] = y - 1./1.2*x + 1;
            }
            if (r[0]*r[1] <= 0 || r[0]*r[2] <= 0 || r2[0]*r2[1] <= 0 || r2[0]*r2[2] <= 0) {
                continue;
            }
        }
        area = element[e].volume;
        for (int i=0; i<4; ++i) {
            diff = x = y = 0;
            for (int j=0; j<3; ++j) {
                idx_node = element[e].node[j];
                diff += lbd[3*i+j]*node[idx_node].Z[2][0];
                x += lbd[3*i+j]*node[idx_node].coordinate[0];
                y += lbd[3*i+j]*node[idx_node].coordinate[1];
            }
            if (cas == 8) {
                shockednozzle(x, y, &rho, vel, &p);
            }
            if (cas == 0) {
                rectangular_reflexion(x, y, &rho, vel, &p);
            }
            else if (cas == -1) {
                rho = x + y;
            }
            else {
                printf("### WARNING can only compute error for problems 8 (shockednozzle) 0 (rectangular_reflection) and -1 (linear), not %d\n", cas);
            }
            diff -= rho;
            diff = fabs(diff);
            errL1 += area*wi[i]*diff;
            errL2 += area*wi[i]*diff*diff;
            errLinf = fmax(diff, errLinf);
        }
    }
    errors[0] = errL1;
    errors[1] = errL2;
    errors[2] = errLinf;
}


void test_error_quadrature()
{
    double x, y, rho, vel[2], p, errors[3], local_area;
    int iVer0;

    for ( int n = 0 ; n < NN ; n++ ) {
        x = node[n].coordinate[0];
        y = node[n].coordinate[1];
        rho = x + y;
        node[n].Z[2][0] = rho;
    }

    compute_error_quadrature(errors, -1);
    if (errors[0] < 1.e-15 || errors[1] < 1.e-15 || errors[2] < 1.e-15) {
        printf("ERROR   Analytic solution was forced, error should be 0, but is: %1.3e %1.3e %1.3e \n", 
                errors[0],errors[1], errors[2] ) ;
    }

    iVer0 = 2000;
    node[iVer0].Z[2][0] += 1;
    local_area = 0;
    for (int e=0; e<NE; ++e) {
        for (int i=0; i<3; ++i) {
            if (element[e].node[i] == iVer0) {
                local_area += element[e].volume;
            }
        }
    }
    compute_error_quadrature(errors, -1);
    if (fabs(errors[0] - local_area) < 1.e-15 || fabs(errors[1]-local_area) < 1.e-15 || fabs(errors[2]-1) < 1.e-15) {
        printf("ERROR   Analytic solution was forced, error should be %1.3e, but is: %1.3e %1.3e %1.3e \n", 
                local_area, errors[0],errors[1], errors[2] ) ;
    }

    printf("Test completed\n");
    exit(1);
}
