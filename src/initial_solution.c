/***************************************************************************
                              initial_solution.c
                              ------------------
This is initial_solution: depending on the value of initial_state, it reads
          or just sets up the initial solution for the computation
                             --------------------
    begin                : Mon May 6 2002
    copyright            : (C) 2002 by Mario Ricchiuto
    email                : ricchiut@vki.ac.be,


***************************************************************************/
#include "common.h"

extern int initial_state ;
extern int NN ;
extern double rho_inf, p_inf, alpha_inf;

extern struct node_struct *node ;

extern void ( *read_solutions )() ;
extern void ( *compute_initial_solution )() ;
extern void P_2_Z( double *, double * ) ;
extern void Z_2_P( double *, double * ) ;

void initial_solution()
{
    int n ;

    switch ( initial_state ) {
    case 0:
        read_solutions() ;
        for ( n = 0 ; n < NN ; n ++ ) {
            P_2_Z( node[n].P[2], node[n].Z[2] ) ;
        }
        break ;
    case 1:
        compute_initial_solution() ;
        break ;
    }
}

void initial_reference_state() {
    for ( int n = 0 ; n < NN ; n ++ ) {
        node[n].Z[2][0] = rho_inf;
        node[n].Z[2][1] = cos(alpha_inf);
        node[n].Z[2][2] = sin(alpha_inf);
        node[n].Z[2][3] = p_inf;
        Z_2_P( node[n].Z[2], node[n].P[2] ) ;
    }
}

/***************************************************************************
 *                                                                         *
 *   This program has been developed at the von Karman Institute for Fluid *
 *   Dynamics by Mario Ricchiuto. Any change or update must be done in a   *
 *   clean way and it must be clearly commented.                           *
 *   Mario Ricchiuto                                                       *
 *                                              23/04/2002                 *
 ***************************************************************************/

