/***************************************************************************
 compute_time_step.c
 -------------------
 This is compute_time_step: it computes the time steps associated to
 first and second time level such that the past shield condition
 is satisfied for a linear scalar equation
 -------------------
 begin                : Mon May 6 2002
 copyright            : (C) 2002 by Mario Ricchiuto
 email                : ricchiut@vki.ac.be
 ***************************************************************************/
#include "common.h"

extern int NE, size, NN , NBF ;

extern struct element_struct *element ;
extern struct node_struct *node ;

extern struct boundary_struct *boundary ;

extern int initial_state, time_step, local_timestepping ;

extern double *temp_vector, *normal, *Lambda ;

extern double dt, shield_factor, time, t_max, q_ratio, gm, zero_r ;

extern void Waves( double * ) ; /* normals, variables, WAVES */
extern void local_average( int ) ;


void compute_time_step()
{
    int e, v, j, w, n, n1, n2, n3 , f ;
    double ratio, delta_t0, volume, max_v, max_c, max_h, speed ;
    double sigma_barx, sigma_bary ;
    FILE *out;
    
    for ( n = 0 ; n < NN ; n ++ ) {
        node[n].dtau = 0. ;
    }
    
    q_ratio = dt ;
    if ( time_step == 1 ) {
        q_ratio = 1. ;
    }
    
    delta_t0 = infinity ;
    for ( e = 0 ; e < NE ; e ++ ) {
        n1 = element[e].node[0] ;
        n2 = element[e].node[1] ;
        n3 = element[e].node[2] ;
        
        sigma_barx = ( node[n1].vel[0] + node[n2].vel[0] + node[n3].vel[0] )/3. ;
        sigma_bary = ( node[n1].vel[1] + node[n2].vel[1] + node[n3].vel[1] )/3. ;
        
        volume = element[e].volume ;
        max_v = max_c = max_h = 0. ;
        for ( v = 0 ; v < 3 ; v ++ ) {
            for ( j = 0 ; j < 2 ; j ++ ) {
                normal[j] = element[e].normal[v][j] ;
            }
            
            volume = sqrt( normal[0]*normal[0] + normal[1]*normal[1] ) ;
            max_h = fmax(volume, max_h);
            
            n = element[e].node[v] ;
            volume = sqrt( (node[n].Z[2][1]-sigma_barx)*(node[n].Z[2][1]-sigma_barx) +
                          + (node[n].Z[2][2]-sigma_bary)*(node[n].Z[2][2]-sigma_bary) ) ;
            max_v = fmax(volume, max_v);
            
            volume = node[n].Z[2][0] ; if ( volume < zero_r ) volume = zero_r ;
            volume = sqrt( gm*node[n].Z[2][3]/volume ) ;
            max_c = fmax(volume, max_c);
        }

        speed = 0.5*( max_v + max_c )*max_h ;
        
        node[element[e].node[0]].dtau += speed ;
        node[element[e].node[1]].dtau += speed ;
        node[element[e].node[2]].dtau += speed ;
    }
    
    for ( f = 0 ; f < NBF ; f ++ ) {
        n1 = boundary[f].node[0] ;
        n2 = boundary[f].node[1] ;
        
        sigma_barx = ( node[n1].vel[0] + node[n2].vel[0] )/2. ;
        sigma_bary = ( node[n1].vel[1] + node[n2].vel[1] )/2. ;
        
        volume = sqrt(boundary[f].normal[0]*boundary[f].normal[0] + boundary[f].normal[1]*boundary[f].normal[1]) ;
        
        max_h = volume ;
        max_v = max_c =  0. ;
        for ( v = 0 ; v < 2 ; v ++ ) {
            n = boundary[f].node[v] ;
            
            volume = sqrt( (node[n].Z[2][1]-sigma_barx)*(node[n].Z[2][1]-sigma_barx) +
                          + (node[n].Z[2][2]-sigma_bary)*(node[n].Z[2][2]-sigma_bary) ) ;
            max_v = fmax(volume, max_v);

            volume = node[n].Z[2][0] ; 
            volume = fmax(volume, zero_r);
            
            volume = sqrt( gm*node[n].Z[2][3]/volume ) ;
            max_c = fmax(volume, max_c);
        }

        speed = 0.5*( max_v + max_c )*max_h ;
        
        node[boundary[f].node[0]].dtau += speed ;
        node[boundary[f].node[1]].dtau += speed ;
    }
    
    for ( n = 0 ; n < NN ; n ++ ) {
        ratio = node[n].vol/( node[n].dtau + 1.e-20 ) ;
        delta_t0 = fmin(delta_t0, ratio);
        node[n].dtau = ratio*shield_factor ;
    }
    
    delta_t0 *= shield_factor ;
    if ( time + delta_t0 > t_max ) {
        delta_t0 = t_max - time ;
    }
    
      
    dt = delta_t0 ;
    for ( n = 0 ; n < NN ; n ++ ) {
        if ( local_timestepping == 0 ) {
            node[n].dtau = dt ;
        }
    }
    
    time += dt ;
    q_ratio /= dt ;
 
}

/***************************************************************************
 *                                                                         *
 *   This program has been developed at the von Karman Institute for Fluid *
 *   Dynamics by Mario Ricchiuto. Any change or update must be done in a   *
 *   clean way and it must be clearly commented.                           *
 *   Mario Ricchiuto                                                       *
 *                                              23/04/2002                 *
 ***************************************************************************/
