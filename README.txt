README

The only test-cases already set in this folder are Q1D-g[012] containing three different
nested grids to analyze the transonic planar source flow. I used the very same grids to
perform the convergence tests that we are supposed to compare afterwards. 

In case you wanted to test something else, you need just to follow the instructions below.

how to use NEO:

src contains the source files of the code. Every parameter is set directly from the test
directory except for the solution value at the boundaries, in case you would like to run
some tests with either subsonic outlet BC (outlet static pressure must be set) or 
far-field BC (the whole solution vector must be set).
If so, you must carry out some modifications within the file "initialize.c".
I already set for you the outlet static pressure needed for the transonic source flow. 

Each test directory contains a "type.dat" file that you may use to change the boundary conditions.
I think you can set a number of boundaries up to 10. The BC flags are the following:

Flag   0 :  Fitted shocks 
Flag   1 :  Supersonic inlet
Flag   2 :  Supersonic outlet 
Flag   3 :  Inviscid wall 
Flag   4 :  Subsonic outlet 
Flag   5 :  Far-field 

In order to run a simulation NEO needs:

1) type.dat                               : for boundary conditions
2) (NEO_data/input/)neogrid.grd           : for the mesh information (each edge must be set with the proper BC flag) *
3) (NEO_data/output/)vvvv_input.dat       : for the initial solution (nodal solution + list of connectivity) **
4) (NEO_data/textinput/)inputfile-exp.txt : for the input information such CFL, scheme, number of time steps, ...

About the 4th file, some brief indications:
1) shield factor  =  CFL 
2) gamma          =  Heat capacity ratio
3) the list of scheme you may use is the following:
   0(LDA),1(LLF),2(LDAN),3(LWLF),4(LW),5(N)  
   at the moment, the scheme LDAN is the one chosen for these simulations 
   because is the one I employed to perform my computations 
4) maximum number of time steps   =  iterations
5) Maximum number of iterations   =  different Runge Kutta schemes for time integration
   at the moment, explicit Euler is set since we're considering steady cases.



MESH READER

* check "read_spatial_grid.c" out to understand how to write this file
** this is a modified dplot ascii format, conversion to .mesh and other formats is not complicated 
*** the main contrain is that the code uses the data as provided by the .grd

SOLUTION READER
* check "read_write.c" ou to understand how solutions are writtent (and read).
** It's an ascii tecplot format but other formats (vtk or other) can be easily inplemented.


IMPORTANT 1: 
if you want to set a uniform initial condition, the only thing you have to do
is using the variables *_inf in "initialize.c" to set you solution vector and then change
initial state from 0 to 15 in "inputfile-exp.txt" 

IMPORTANT 2:
Each directory "NEO_data/input" contains a file called "vel.dat". it is needed to store the 
grid velocity in case of ALE simulations. This is not the case therefore it contains a list of zeroes.
Nevertheless this file is needed by NEO to run.
